﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace DataTransferObject
{
    public class DataBaseConnection
    {
        private readonly IDataAccess _dataAccess;
        public DataBaseConnection(IDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public List<Cost> GetCosts()
        {
            return _dataAccess.GetAllCosts();
        }

        public Cost GetCost(int id)
        {
            return _dataAccess.GetSingleCostById(id);
        }

        public void InsertCost(Cost cost)
        {
            _dataAccess.InsertSingleCost(cost);
        }

        public void InsertCosts(List<Cost> costs)
        {
            _dataAccess.InsertMultipleCosts(costs);
        }

        public void UpdateCost(Cost cost)
        {
            _dataAccess.UpdateSingleCost(cost);
        }

        public void RemoveCost(int id)
        {
            _dataAccess.RemoveSingleCostById(id);
        }

        #region Sorting
        public List<Cost> SortByPayDate()
        {
            return GetCosts().OrderBy(d => d.PayDate).ToList();
        }

        #endregion
    }
}