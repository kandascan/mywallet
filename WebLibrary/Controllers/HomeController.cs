﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess;
using DataTransferObject;
using WebLibrary.Models;

namespace WebLibrary.Controllers
{
    public class KindCost
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
    public class HomeController : Controller
    {
        private readonly MongoConnection _mongoConnection;
        private readonly DataBaseConnection _dataBase;

        public HomeController()
        {
            _mongoConnection = new MongoConnection("MyWalletDBBeta");
            _dataBase = new DataBaseConnection(_mongoConnection);
        }
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult DDLCost()
        {
            return Json(new List<KindCost>
            {
                new KindCost() {Id = 1, Value = "Paliwo"},
                new KindCost() {Id = 2, Value = "Jedzenie"},
                new KindCost() {Id = 3, Value = "Uzywki"},
                new KindCost() {Id = 4, Value = "Szkola"},
                new KindCost() {Id = 5, Value = "Praca"}
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCosts()
        {
            return Json(_dataBase.GetCosts(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCostDataForChartPie()
        {
            var data = _dataBase.GetCosts();

            var chartData = new object[data.Count() + 1];
            chartData[0] = new object[]
            {
                "Product Name",
                "Product Price"
            };

            var j = 0;
            foreach (var i in data)
            {
                j++;
                chartData[j] = new object[] { i.CostName, Convert.ToInt16(i.CostPrice) };
            }
            return new JsonResult { Data = chartData, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetCostDataForAreaChart()
        {
            var data = _dataBase.SortByPayDate().GroupBy(l => l.PayDate)
                .Select(g => new
                {
                    PayDate = g.Key,
                    CostPrice = g.Select(l => l.CostPrice).Distinct().Sum()
                });

            var chartData = new object[data.Count() + 1];
            chartData[0] = new object[]
            {
                "Date",
                "Product Price"
            };

            var j = 0;
            foreach (var i in data)
            {
                j++;
                chartData[j] = new object[] { i.PayDate.ToString("M", CultureInfo.CurrentCulture), Convert.ToInt16(i.CostPrice) };
            }
            return new JsonResult { Data = chartData, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public void InsertCost(CostModel costModel)
        {
            costModel.Id = _dataBase.GetCosts().Count == 0 ? 1 : _dataBase.GetCosts().Count + 1;
            var singleCost = new Cost
            {
                Id = costModel.Id,
                CostName = costModel.CostName,
                CostKind = costModel.CostKind,
                CostNotes = costModel.CostNotes,
                CostPrice = float.Parse(costModel.CostPrice, CultureInfo.InvariantCulture.NumberFormat),
                PayDate = Convert.ToDateTime(costModel.PayDate)
            };
            _dataBase.InsertCost(singleCost);
        }
    }
}