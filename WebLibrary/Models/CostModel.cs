﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLibrary.Models
{
    public class CostModel
    {
        public int Id { get; set; }
        public string CostName { get; set; }
        public string CostPrice { get; set; }
        public string CostNotes { get; set; }
        public string PayDate { get; set; }
        public string CostKind { get; set; }
    }
}