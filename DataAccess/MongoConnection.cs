﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace DataAccess
{
    public partial class MongoConnection : IDataAccess
    {
        private MongoDatabase MongoDatabase { get; set; }
        private MongoClient MongoClient { get; set; }
        private MongoClientSettings Settings { get; set; }

        private const string ConnectionString = "mongodb://localhost:27017";

        public MongoConnection(string dataBaseName)
        {
            Settings = MongoClientSettings.FromUrl(new MongoUrl(ConnectionString));
            MongoClient = new MongoClient(Settings);
            var server = MongoClient.GetServer();
            MongoDatabase = server.GetDatabase(dataBaseName);
        }

        public bool CheckMongoConnection()
        {
            var server = MongoClient.GetServer();
            try
            {
                server.Connect();
                server.Disconnect();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        #region Select
        public List<Cost> GetAllCosts()
        {
            return MongoDatabase.GetCollection<Cost>("Cost").FindAll().ToList();
        }

        public Cost GetSingleCostById(int id)
        {
            return MongoDatabase.GetCollection<Cost>("Cost").FindOneById(id);
        }
        #endregion
        #region Insert
        public void InsertSingleCost(Cost singleCost)
        {
            var collection = MongoDatabase.GetCollection<Cost>("Cost");
            collection.Insert(singleCost);
        }

        public void InsertMultipleCosts(List<Cost> costs)
        {
            var collection = MongoDatabase.GetCollection<Cost>("Cost");
            collection.InsertBatch(costs);
        }
        #endregion
        #region Update
        public void UpdateSingleCost(Cost singleCost)
        {
            var collection = MongoDatabase.GetCollection<Cost>("Cost");
            var query = Query.EQ("_id", singleCost.Id);
            var update = Update.Set("CostName", singleCost.CostName)
                .Set("CostPrice", singleCost.CostPrice).Set("CostNotes", singleCost.CostNotes);
            collection.FindAndModify(query, SortBy.Null, update);
        }
        #endregion
        #region Remove
        public void RemoveSingleCostById(int id)
        {
            var collection = MongoDatabase.GetCollection<Cost>("Cost");
            collection.Remove(Query.EQ("_id", id));
        }
        #endregion
    }
}
