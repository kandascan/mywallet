﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public interface IDataAccess
    {
        bool CheckMongoConnection();
        List<Cost> GetAllCosts();
        Cost GetSingleCostById(int id);
        void InsertSingleCost(Cost singleCost);
        void InsertMultipleCosts(List<Cost> costs);
        void UpdateSingleCost(Cost singleCost);
        void RemoveSingleCostById(int id);
    }
}
