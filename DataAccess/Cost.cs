﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class Cost
    {
        public int Id { get; set; }
        public string CostName { get; set; }
        public float CostPrice { get; set; }
        public string CostNotes { get; set; }
        public DateTime PayDate { get; set; }
        public string CostKind { get; set; }

    }
}
