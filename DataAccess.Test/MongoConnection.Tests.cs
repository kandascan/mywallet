﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace DataAccess.Test
{
    [TestFixture]
    public class MongoConnectionTests
    {
        [Test]
        public void CheckMongoConnection()
        {
            var mongoConnection = new MongoConnection("MyWalletDBBeta");
            var isMongoConnection = mongoConnection.CheckMongoConnection();

            Assert.IsTrue(isMongoConnection);
        }
        
        [Test]
        public void InsertAndRemoveSingleCostToDataBase()
        {
            var mongoConnection = new MongoConnection("MyWalletDBBeta");
            mongoConnection.InsertSingleCost(new Cost
            {
                Id = -1,
                CostName = "Mleko",
                CostPrice = (float) 2.75,
                CostNotes = "Mleko 3,2%"
            });

            var cost = mongoConnection.GetAllCosts();

            Assert.That(cost.Any(x => x.Id == -1));
            Assert.That(cost.Count, Is.GreaterThan(0));

            mongoConnection.RemoveSingleCostById(-1);
            cost = mongoConnection.GetAllCosts();

            Assert.That(cost.Count(x => x.Id == -1), Is.EqualTo(0));
            Assert.That(cost, Is.All.Not.Null);
            Assert.That(cost, Is.All.AssignableFrom(typeof(Cost)));
        }
        /*
        [Test]
        public void UpdateSchoolSubjectOntoDataBase()
        {
            var mongoConnection = new MongoConnection("ScheduleDataBaseBeta");
            mongoConnection.InsertSchoolSubject(new SchoolSubject
            {
                Id = -1,
                SubjectName = "Angielski",
                ClassNumber = "A12"
            });

            mongoConnection.UpdateSchoolSubject(new SchoolSubject
            {
                Id = -1,
                SubjectName = "Polski",
                ClassNumber = "C31"
            });

            var schoolSubject = mongoConnection.GetSchoolSubjectById(-1);

            Assert.AreEqual("Polski", schoolSubject.SubjectName);
            Assert.AreEqual("C31", schoolSubject.ClassNumber);

            mongoConnection.RemoveSchoolSubjectById(-1);
            var schoolSubjectCollection = mongoConnection.GetListOfSchoolSubjects();

            Assert.That(schoolSubjectCollection.Count(s => s.Id == -2), Is.EqualTo(0));
            Assert.That(schoolSubjectCollection, Is.All.Not.Null);
            Assert.That(schoolSubjectCollection, Is.All.AssignableFrom(typeof(SchoolSubject)));
        }

        [TestCase("Polski", 1)]
        [TestCase("Matematyka", 2)]
        [TestCase("Plastyka", 4)]
        public void GetElementById(string subjectName, int id)
        {
            var subjectList = new List<SchoolSubject>
            {
                new SchoolSubject {Id = 1, SubjectName = "Polski", ClassNumber = "12"},
                new SchoolSubject {Id = 2, SubjectName = "Matematyka", ClassNumber = "32"},
                new SchoolSubject {Id = 4, SubjectName = "Plastyka", ClassNumber = "24"}
            };

            var mongoConnection = new MongoConnection("ScheduleDataBaseBeta");
            mongoConnection.InsertSchoolSubjectList(subjectList);

            var schoolSubject = mongoConnection.GetSchoolSubjectById(id);

            Assert.AreEqual(subjectName, schoolSubject.SubjectName);
            Assert.AreEqual(id, schoolSubject.Id);

            mongoConnection.RemoveSchoolSubjectById(1);
            mongoConnection.RemoveSchoolSubjectById(2);
            mongoConnection.RemoveSchoolSubjectById(4);

            var schoolSubjectList = mongoConnection.GetListOfSchoolSubjects();

            Assert.That(schoolSubjectList.Count, Is.EqualTo(0));
            Assert.That(schoolSubjectList, Is.All.Null);
            Assert.That(schoolSubjectList, Is.All.AssignableFrom(typeof(SchoolSubject)));
        }*/
    }
}
